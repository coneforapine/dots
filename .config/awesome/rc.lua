require("variables")
require("evil")
require("awful.autofocus")
local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")
local utils = require("utils")

local keys = require("keys")
local rules = require("rules")

beautiful.init(string.format("%s/.config/awesome/theme/theme.lua", os.getenv("HOME")))

local widgets = require("widgets")
awful.util.terminal = terminal

local theme = beautiful.get()

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
  naughty.notify({ preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
  text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal("debug::error", function (err)
    if in_error then return end
    in_error = true

    naughty.notify({ preset = naughty.config.presets.critical,
      title = "Oops, an error happened!",
    text = tostring(err) })
    in_error = false
  end)
end
-- }}}

-- {{{ Screen
-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", function(s)
    awful.spawn.with_shell('feh --bg-scale /home/awesomey/.config/wallpaper.png')
end)

-- restore wallaper on restarting (mod shift f2)
do
  awesome.connect_signal("refresh", function(a)
    awful.spawn.with_shell('feh --bg-scale /home/awesomey/.config/wallpaper.png')
  end)
end


-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s)

    awful.spawn.with_shell('feh --bg-scale /home/awesomey/.config/wallpaper.png')
  -- Tags
  --awful.tag(awful.util.tagnames, s, awful.layout.layouts[1])
  for _, i in pairs(awful.util.tagnames[s.index]) do
    awful.tag.add(i.name, {
      layout = i.lay or awful.layout.layouts[1],
      gap = i.gap or beautiful.useless_gap,
      gap_single_client = not i.sgap,
      screen = s,
      selected = i.sel or false,
      master_width_factor = i.mw or 0.5,
    })
  end

  -- Create a promptbox for each screen
  -- Create an imagebox widget which will contains an icon indicating which layout we're using.
  -- We need one layoutbox per screen.
  s.mylayoutbox = awful.widget.layoutbox(s)
  s.mylayoutbox:buttons(gears.table.join(
    awful.button({}, 2, function() awful.layout.set( awful.layout.layouts[1] ) end),
    awful.button({}, 4, function() awful.layout.inc( 1) end),
    awful.button({}, 5, function() awful.layout.inc(-1) end)))
  -- Create a taglist widget
  s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

  -- Create a tasklist widget
  s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags)

  -- Create the wibox
--[[  s.mywibox = awful.wibar({ 
	  position = "top",
	  screen = s, 
	  height = 30,
  })

  s.mywibox.visible = false
--]]

  s.taskbox = wibox{
    width = 300,
    height = 30,
    screen = s,
    visible = true
  }
  
  -- this one is tricky
  -- 
  s.infobox = wibox{
    width = 200,
    height = 30,
    screen = s,
    visible = true,
    bg = "#FFFFFF00"
  }

  s.tasklistbox = wibox{
    width = 200,
    height = 40,
    screen = s,
    visible = true,
    bg = "#FFFFFF00"
  }

  s.taskbox:setup {
    layout = wibox.layout.align.horizontal,
    spacing = 1,
    s.mytaglist,
    widgets.space,
    widgets.textclock
  }

  s.infobox:setup{
    layout = wibox.layout.align.horizontal,
    { -- bat, kblayout, vol and maybe a tiny clock.
      widget = wibox.container.margin,
      {
        widget = wibox.container.background,
        bg = theme.bg_normal,
        {
          layout = wibox.layout.fixed.horizontal,
          widgets.space,
          widgets.bat,
          widgets.kblayout,
          widgets.vol
        }
      }
    },
    nil,
    { -- systray area
      widget = wibox.container.margin,
      right = 10,
      {
        widget = wibox.container.background,
        bg = theme.bg_normal,
        {
          layout = wibox.layout.fixed.horizontal,
          wibox.widget.systray(true),
          widgets.space
        }
      },
    }
  }

  s.tasklistbox:setup {
    layout = wibox.layout.fixed.horizontal,
    s.mytasklist
  }
  struts = {
    top = 0,
    right = 0,
    left = 0,
    bottom = 50 
  }

  -- big bruh moment here.
  s.infobox:struts(struts)
  s.taskbox:struts(struts)
  s.tasklistbox:struts(struts)

  local screenshape = s.geometry
  s.taskbox.x = (screenshape.width / 100) * 20 - s.taskbox.width
  s.taskbox.y = (screenshape.height / 100) * 99 - s.taskbox.height
  s.infobox.x = (screenshape.width / 100 ) * 96 - s.infobox.width 
  s.infobox.y = (screenshape.height / 100) * 99 - s.infobox.height
  s.tasklistbox.x = (screenshape.width / 100) * 55 - s.tasklistbox.width
  s.tasklistbox.y = (screenshape.height/ 100) * 99 - s.tasklistbox.height

  --[[
  -- Add widgets to the wibox
  s.mywibox:setup {
    layout = wibox.layout.align.horizontal,
    expand = 'none',
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      s.mylayoutbox,
      s.mytaglist,
      s.mypromptbox,
      widgets.seperator,
      widgets.media,
    },
      wibox.container.place(widgets.textclock, "center"),
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      id = "rightwidgets",
      wibox.widget.systray(),
      seperator,
      widgets.kblayout,
      widgets.temp,
      widgets.disk,
      seperator,
      widgets.cpu,
      widgets.mem,
      widgets.vol,
      widgets.bat,
      widgets.space,
    },
  }

  --]]
end)
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
  -- Start the new client as slave.
  if not awesome.startup then awful.client.setslave(c) end

  if awesome.startup and
  not c.size_hints.user_position
  and not c.size_hints.program_position then
    -- Prevent clients from being unreachable after screen count changes.
    awful.placement.no_offscreen(c, {honor_padding = true})
  end
end)


-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
  c:emit_signal("request::activate", "mouse_enter", {raise = true})
end)


-- Force minimized clients to unminimize.
client.connect_signal("property::minimized", function(c)
  c.minimized = false
end)


client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- Quake terminal
quake = utils.quake {
  app = terminal,
  horiz = "center",
  height = 0.4,
  width = 0.9,
  followtag = true,
  border = 0
}

